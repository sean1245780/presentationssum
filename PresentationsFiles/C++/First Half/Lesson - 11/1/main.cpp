#include "threads.h"

using std::vector;

int main()
{
	call_I_Love_Threads();

	vector<int> primes1;
	getPrimes(58, 100, primes1);

	vector<int> primes3 = callGetPrimes(93, 289);
	
	vector<int> p1 = callGetPrimes(0, 1000);
	vector<int> p2 = callGetPrimes(0, 100000);
	vector<int> p3 = callGetPrimes(0, 1000000);
	std::cout << "p1: " << p1.size() << std::endl;
	printVector(p1);

	std::cout << "Till 1000:" << std::endl;
	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 2);
	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 4);
	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 6);
	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 12);
	std::cout << "Till 100000:" << std::endl;
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", 2);
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", 4);
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", 6);
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", 12);
	std::cout << "Till 1000000:" << std::endl;
	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 2);
	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 4);
	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 6);
	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 12);
	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 15);
	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 20);
	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 50);

	system("pause");
	return 0;
}