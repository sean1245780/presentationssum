#include <string>
#include <fstream>
#include <iostream>
#include <mutex>

void writePrimesToFile(int begin, int end, std::ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);
