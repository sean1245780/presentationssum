#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "InputException.h"

int main()
{
	std::string nam, col; double rad = 5, ang = 5, ang2 = 175; int height = 1, width = 1;

	try
	{
		Circle circ(col, nam, rad);
		quadrilateral quad(nam, col, width, height);
		rectangle rec(nam, col, width, height);
		parallelogram para(nam, col, width, height, ang, ang2);

		Shape* ptrcirc = &circ;
		Shape* ptrquad = &quad;
		Shape* ptrrec = &rec;
		Shape* ptrpara = &para;


		std::cout << "Enter information for your objects" << std::endl;
		const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; char shapetype;
		char x = 'y';
		while (x != 'x') {
			std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram" << std::endl;
			std::cin >> shapetype;
			cin_check();
			try
			{

				switch (shapetype) {
				case 'c':
					std::cout << "enter color, name,  rad for circle" << std::endl;
					std::cin >> col >> nam >> rad;
					cin_check();
					circ.setColor(col);
					circ.setName(nam);
					circ.setRad(rad);
					ptrcirc->draw();
					break;
				case 'q':
					std::cout << "enter name, color, height, width" << std::endl;
					std::cin >> nam >> col >> height >> width;
					quad.setName(nam);
					quad.setColor(col);
					quad.setHeight(height);
					quad.setWidth(width);
					ptrquad->draw();
					break;
				case 'r':
					std::cout << "enter name, color, height, width" << std::endl;
					std::cin >> nam >> col >> height >> width;
					rec.setName(nam);
					rec.setColor(col);
					rec.setHeight(height);
					rec.setWidth(width);
					ptrrec->draw();
					break;
				case 'p':
					std::cout << "enter name, color, height, width, 2 angles" << std::endl;
					std::cin >> nam >> col >> height >> width >> ang >> ang2;
					para.setName(nam);
					para.setColor(col);
					para.setHeight(height);
					para.setWidth(width);
					para.setAngle(ang, ang2);
					ptrpara->draw();

				default:
					std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
					break;
				}
				std::cout << "would you like to add more object press any key if not press x" << std::endl;
				std::cin.get() >> x;
			}
			catch (std::exception & e)
			{
				printf(e.what());
			}
			catch (...)
			{
				printf("caught a bad exception. continuing as usual\n");
			}
		}
	}
	catch (std::exception & e)
	{
		std::cerr << e.what() << std::endl;
	}



		system("pause");
		return 0;
	
}

void cin_check()
{
	if (std::cin.fail())
	{
		std::cin.clear();
		std::cin.ignore();
	}
}