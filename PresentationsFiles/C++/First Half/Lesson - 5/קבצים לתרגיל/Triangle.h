#include "Polygon.h"
#include <string>

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
	virtual ~Triangle();
	
	// override functions if need (virtual + pure virtual)
};