#pragma once

#include "Piece.h"
#include "ChessException.h"

class Rook : public Piece
{
	public:
		Rook(const Type& pieceType, const std::string& pieceLocation, const Color& pieceColor);
		virtual ~Rook();

		virtual void move(const std::string& newPlace, Piece*** board, Piece** pieceToDelete);
};