#pragma once

#include "Piece.h"
#include "ChessException.h"

class Knight : public Piece
{
	public:
		Knight(const Type& pieceType, const string& pieceLocation, const Color& pieceColor);
		virtual ~Knight();

		virtual void move(const std::string& newPlace, Piece*** board, Piece** pieceToDelete);
};