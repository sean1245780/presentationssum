#include "King.h"

King::King(const Type& pieceType, const std::string& pieceLocation, const Color& pieceColor):Piece(pieceType, pieceLocation, pieceColor)
{
	// Piece is taking care of
}

King::~King()
{
	// system cleans up automaticlly
}

void King::move(const string& newPlace, Piece*** board, Piece** pieceToDelete)
{
	if (_pieceLocation == newPlace)
	{
		throw ChessException(Move::SRC_EQU_DEST);
	}

	if (newPlace[SRC_CHAR] == _pieceLocation[SRC_CHAR] && newPlace[SRC_INT] == _pieceLocation[SRC_INT] + 1 || // move forward
		newPlace[SRC_CHAR] == _pieceLocation[SRC_CHAR] && newPlace[SRC_INT] == _pieceLocation[SRC_INT] - 1 || // move backwards
		newPlace[SRC_CHAR] == char(int(_pieceLocation[SRC_CHAR]) + 1) && newPlace[SRC_INT] == _pieceLocation[SRC_INT] || // move right
		newPlace[SRC_CHAR] == char(int(_pieceLocation[SRC_CHAR]) - 1) && newPlace[SRC_INT] == _pieceLocation[SRC_INT] || // move left
		newPlace[SRC_CHAR] == char(int(_pieceLocation[SRC_CHAR]) + 1) && newPlace[SRC_INT] == _pieceLocation[SRC_INT] + 1 || // right upper corner
		newPlace[SRC_CHAR] == char(int(_pieceLocation[SRC_CHAR]) - 1) && newPlace[SRC_INT] == _pieceLocation[SRC_INT] + 1 || // left upper corner
		newPlace[SRC_CHAR] == char(int(_pieceLocation[SRC_CHAR]) + 1) && newPlace[SRC_INT] == _pieceLocation[SRC_INT] - 1 || // right bottom corner
		newPlace[SRC_CHAR] == char(int(_pieceLocation[SRC_CHAR]) - 1) && newPlace[SRC_INT] == _pieceLocation[SRC_INT] - 1) // left bottom corner
	{
		_pieceLocation =  newPlace;
		throw ChessException(Move::VALID);
	}

	else
	{
		throw ChessException(Move::INVALID_MOVE_PATTERN);
	}
}