#pragma once

#include "Piece.h"
#include "ChessException.h"

class Pawn : public Piece
{
	public:
		Pawn(const Type& pieceType, const string& pieceLocation, const Color& pieceColor);
		virtual ~Pawn();

		virtual void move(const std::string& newPlace, Piece*** board, Piece** pieceToDelete);

	private:
		bool hasMoved;
};

