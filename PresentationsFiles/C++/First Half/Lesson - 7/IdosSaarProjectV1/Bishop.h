#pragma once
#include "Piece.h"
#include "chessException.h"

class Bishop : public Piece
{
	public:
		Bishop(const Type& pieceType, const string& pieceLocation, const Color& pieceColor);
		virtual ~Bishop();

		virtual void move(const std::string& newPlace, Piece*** board, Piece** pieceToDelete);
};