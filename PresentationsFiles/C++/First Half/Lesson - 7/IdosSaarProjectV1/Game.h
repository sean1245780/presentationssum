#pragma once

#include "GameBoard.h"
#include "Pipe.h"
#include <thread>

#define EXIT_MSG "quit"
#define WAIT_5_SEC 5000

using std::string;

class Game
{
	public:
		Game(const Color& startingColor = Color::WHITE);
		~Game();

		void start();

	private:
		Pipe _gui;
		Color _turn;
		GameBoard _gameBoard;
		//Player blackPiecesPlayer;
		//Player whitePiecesPlayer;	
};