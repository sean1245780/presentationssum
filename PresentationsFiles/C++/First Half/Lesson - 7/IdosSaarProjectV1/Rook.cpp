#include "Rook.h"

Rook::Rook(const Type& pieceType, const std::string& pieceLocation, const Color& pieceColor):Piece(pieceType, pieceLocation, pieceColor)
{
	// Piece is taking care of
}

Rook::~Rook()
{
	// system cleans up automaticlly
}

void Rook::move(const std::string& newPlace, Piece*** board, Piece**)
{
	if (_pieceLocation != newPlace)
	{
		if (_pieceLocation[SRC_CHAR] == newPlace[SRC_CHAR] || _pieceLocation[SRC_INT] == newPlace[SRC_INT])
		{
			_pieceLocation = newPlace;
			throw ChessException(Move::VALID);
		}
		else
		{
			throw ChessException(Move::INVALID_MOVE_PATTERN);
		}
	}
	else
	{
		throw ChessException(Move::SRC_EQU_DEST);
	}
}