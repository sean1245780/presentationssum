#pragma once

#include "Piece.h"
#include "ChessException.h"

using std::string;

class King : public Piece
{
	public:
		King(const Type& pieceType, const std::string& pieceLocation, const Color& pieceColor);
		virtual ~King();

		virtual void move(const string& newPlace, Piece*** board, Piece** pieceToDelete);
};