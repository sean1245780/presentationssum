#include "Game.h"

Game::Game(const Color& startingColor):_gameBoard(startingColor)
{
	this->_turn = startingColor;

	bool connected = this->_gui.connect();

	while (!connected)
	{
		Sleep(WAIT_5_SEC);
		connected = this->_gui.connect();
	}
}

Game::~Game()
{
	this->_gui.close();
}

void Game::start()
{
	string msgFromGui;
	string boardStr = this->_gameBoard.getBoardStr();
	char* msgToGui = new char[boardStr.size() + 1];

	std::copy(boardStr.begin(), boardStr.end(), msgToGui);
	msgToGui[boardStr.size()] = '\0';

	this->_gui.sendMessageToGraphics(msgToGui);

	while ((msgFromGui = this->_gui.getMessageFromGraphics()) != EXIT_MSG)
	{
		this->_gameBoard.makeMove(msgFromGui);
		this->_gui.sendMessageToGraphics(msgToGui);
	}

	delete[] msgToGui;
}
