#include "Bishop.h"

Bishop::Bishop(const Type& pieceType, const string& pieceLocation, const Color& pieceColor):Piece(pieceType, pieceLocation, pieceColor)
{

}

Bishop::~Bishop()
{
	// system cleans up automaticlly
}

void Bishop::move(const std::string& newPlace, Piece*** board, Piece** pieceToDelete)
{
	if (_pieceLocation == newPlace)
	{
		throw ChessException(Move::SRC_EQU_DEST);
	}
	else
	{
		if (int(_pieceLocation[SRC_CHAR]) - int(newPlace[SRC_CHAR]) == _pieceLocation[SRC_INT] - newPlace[SRC_INT] || // slant right 
			int(newPlace[SRC_CHAR]) - int(_pieceLocation[SRC_CHAR]) == _pieceLocation[SRC_INT] - newPlace[SRC_INT]) // slant left
		{
			_pieceLocation = newPlace;
			throw ChessException(Move::VALID);
		}
		else
		{
			throw ChessException(Move::INVALID_MOVE_PATTERN);
		}
	}
}