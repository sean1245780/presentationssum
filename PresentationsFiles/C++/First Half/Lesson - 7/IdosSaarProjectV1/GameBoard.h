#pragma once

#include "ChessException.h"
#include "Piece.h"
#include "Rook.h"
#include "King.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "Pawn.h"
#include <algorithm>

#define BOARD_START 1
#define BOARD_SIZE 8 
#define STR_STARTING_PLAYER_LOC 64

#define SPECIAL_WHITE 0
#define SPECIAL_BLACK 7

#define PAWNS_WHITE 1
#define PAWNS_BLACK 6

#define EMPTY_SPACE_START 2
#define EMPTY_SPACE_END 5

using std::string;

class GameBoard
{
	public:
		GameBoard(const Color& startingColor);
		~GameBoard();

		void draw() const;
		void deletePiece(Piece* pieceToDelete);
		void makeMove(const string& move);

		Piece*** getBoard() const;
		string getBoardStr() const;

		Piece* getPieceByLocation(const string& location) const;
		bool isKingChecked(const Color& pieceColor) const;
		
		void inBounds(const string& location) const;
		void destHasOppositeColor(const string& location, const Color& currPlayingColor) const;
		void srcHasWantedColor(const string& location, const Color& currPlayingColor) const;
		void willMoveMakeKingChecked(const string& move, const Color& currPlayingColor) const;
		

	private:
		Piece*** _board;
		string _boardString;
};