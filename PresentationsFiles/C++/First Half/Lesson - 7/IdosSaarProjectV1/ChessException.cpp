#include "ChessException.h"

ChessException::ChessException(const Move& code)
{
	this->returnCode = code;
}

ChessException::~ChessException()
{

}

Move ChessException::getExceptionCode() const
{
	return this->returnCode;
}
