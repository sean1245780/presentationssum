#include "GameBoard.h"

GameBoard::GameBoard(const Color& startingColor)
{
	string pieceLocation = "";
	Color pieceColor = Color::WHITE;
	Type pieceType = Type::Rook;
	bool emptyRow = false;

	this->_boardString = "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0";
	this->_boardString[STR_STARTING_PLAYER_LOC] = int(startingColor) + '0'; // make the color a char
	
	this->_board = new Piece**[BOARD_SIZE];

	for (int i = 0; i < BOARD_SIZE; i++)
	{
		this->_board[i] = new Piece*[BOARD_SIZE]; // row
	}

	for (int j = 0; j < BOARD_SIZE; j++)
	{	
		for (int i = 0; i < BOARD_SIZE; i++)
		{
			if (i >= EMPTY_SPACE_START && i <= EMPTY_SPACE_END)
			{
				emptyRow = true;
			}
			else
			{
				emptyRow = false;

				if (i == SPECIAL_WHITE || i == PAWNS_WHITE)
				{
					pieceColor = Color::WHITE;
				}
				else if (i == SPECIAL_BLACK || i == PAWNS_BLACK)
				{
					pieceColor = Color::BLACK;
				}
			}
			
			pieceLocation.clear();
			pieceLocation.push_back(char(j + 'a'));
			pieceLocation.push_back(char(i + 1 + '0'));

			if (emptyRow)
			{
				this->_board[i][j] = nullptr;
			}
			else if (i == PAWNS_WHITE || i == PAWNS_BLACK)
			{
				pieceType = Type::Pawn;

				this->_board[i][j] = new Pawn(pieceType, pieceLocation, pieceColor);
			}
			else if (j == int(SpecialPiecesSL::ROOK_COLUMN_1) || j == int(SpecialPiecesSL::ROOK_COLUMN_2))
			{
				pieceType = Type::Rook;
				
				this->_board[i][j] = new Rook(pieceType, pieceLocation, pieceColor);
			}
			else if (j == int(SpecialPiecesSL::KNIGHT_COLUMN_1) || j == int(SpecialPiecesSL::KNIGHT_COLUMN_2))
			{
				pieceType = Type::Knight;

				this->_board[i][j] = new Knight(pieceType, pieceLocation, pieceColor);
			}
			else if (j == int(SpecialPiecesSL::BISHOP_COLUMN_1) || j == int(SpecialPiecesSL::BISHOP_COLUMN_2))
			{
				pieceType = Type::Bishop;

				this->_board[i][j] = new Bishop(pieceType, pieceLocation, pieceColor);
			}
			else if (j == int(SpecialPiecesSL::QUEEN_COLUMN))
			{
				pieceType = Type::Queen;

				this->_board[i][j] = new Queen(pieceType, pieceLocation, pieceColor);
			}
			else if (j == int(SpecialPiecesSL::KING_COLUMN))
			{
				pieceType = Type::King;

				this->_board[i][j] = new King(pieceType, pieceLocation, pieceColor);
			}
		}
	}
}

GameBoard::~GameBoard()
{
	// system cleans up automaticlly
}

void GameBoard::draw() const
{
	char displayBoard[35][57] = {
	{' ', ' ', ' ', ' ', ' ', ' ', ' ', 'A', ' ', ' ', ' ', ' ', ' ', 'B', ' ', ' ', ' ', ' ', ' ', 'C', ' ', ' ', ' ', ' ', ' ', 'D', ' ', ' ', ' ', ' ', ' ', 'E', ' ', ' ', ' ', ' ', ' ', 'F', ' ', ' ', ' ', ' ', ' ', 'G', ' ', ' ', ' ', ' ', ' ', 'H', ' ', ' ', ' '},
	{' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', '8', ' ', '#', ' ', ' ', 'r', ' ', ' ', '#', ' ', ' ', 'n', ' ', ' ', '#', ' ', ' ', 'b', ' ', ' ', '#', ' ', ' ', 'q', ' ', ' ', '#', ' ', ' ', 'k', ' ', ' ', '#', ' ', ' ', 'b', ' ', ' ', '#', ' ', ' ', 'N', ' ', ' ', '#', ' ', ' ', 'R', ' ', ' ', '#', ' ', '8', ' ', ' '},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', '7', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', '7', ' ', ' '},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', '6', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', '6', ' ', ' '},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', '5', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', '5', ' ', ' '},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', '4', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', '4', ' ', ' '},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', '3', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', '3', ' ', ' '},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', '2', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', ' ', 'P', ' ', ' ', '#', ' ', '2', ' ', ' '},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', '1', ' ', '#', ' ', ' ', 'R', ' ', ' ', '#', ' ', ' ', 'N', ' ', ' ', '#', ' ', ' ', 'B', ' ', ' ', '#', ' ', ' ', 'Q', ' ', ' ', '#', ' ', ' ', 'K', ' ', ' ', '#', ' ', ' ', 'B', ' ', ' ', '#', ' ', ' ', 'N', ' ', ' ', '#', ' ', ' ', 'R', ' ', ' ', '#', ' ', '1', ' ', ' '},
	{' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#'},
	{' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
	{' ', ' ', ' ', ' ', ' ', ' ', ' ', 'A', ' ', ' ', ' ', ' ', ' ', 'B', ' ', ' ', ' ', ' ', ' ', 'C', ' ', ' ', ' ', ' ', ' ', 'D', ' ', ' ', ' ', ' ', ' ', 'E', ' ', ' ', ' ', ' ', ' ', 'F', ' ', ' ', ' ', ' ', ' ', 'G', ' ', ' ', ' ', ' ', ' ', 'H', ' ', ' ', ' '}
	};

	for (int i = 0; i < 35; i++)
	{
		std::cout << std::endl;

		for (int j = 0; j < 57; j++)
		{
			if (displayBoard[i][j] != '\0')
			{
				std::cout << displayBoard[i][j];
			}
		}
	}

	std::cout << std::endl << std::endl;
}

void GameBoard::deletePiece(Piece* pieceToDelete)
{
	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			if (pieceToDelete == this->_board[i][j])
			{
				delete this->_board[i][j];
			}
		}
	}
}

void GameBoard::makeMove(const string& move)
{
	Piece* pieceToDelete = nullptr;
	Piece* pieceToMove = nullptr;
	string srcMove = "";
	string destMove = "";

	srcMove.push_back(move[SRC_CHAR]);
	srcMove.push_back(move[SRC_INT]);
	destMove.push_back(move[DEST_CHAR]);
	destMove.push_back(move[DEST_INT]);

	pieceToMove = this->getPieceByLocation(srcMove);

	try
	{
		pieceToMove->move(destMove, this->_board, &pieceToDelete);
	}
	catch (const ChessException & error)
	{
		if (error.getExceptionCode() == Move::VALID && pieceToDelete != nullptr)
		{
			this->deletePiece(pieceToDelete);
		}

		throw error;
	}
}

Piece*** GameBoard:: getBoard() const
{
	return _board;
}

string GameBoard::getBoardStr() const
{
	return this->_boardString;
}

Piece* GameBoard::getPieceByLocation(const string& location) const
{
	return _board[int(location[SRC_CHAR]) - '`' - 1][int(location[SRC_INT]) - '0' - 1];
}

bool GameBoard::isKingChecked(const Color& pieceColor) const
{
	Piece* king = nullptr;
	Piece* toDeleteTemp = nullptr;
	string originalPieceLocation;

	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			if (this->_board[i][j]->getPieceType() == Type::King && this->_board[i][j]->getPieceColor() == pieceColor)
			{
				king = this->_board[i][j];
			}
		}
	}

	for (int i = 0; i < BOARD_SIZE; i++)
	{
		for (int j = 0; j < BOARD_SIZE; j++)
		{
			if (this->_board[i][j]->getPieceColor() != pieceColor) // if its from the opposite team...
			{
				try
				{
					originalPieceLocation = this->_board[i][j]->getPieceLocation();
					this->_board[i][j]->move(king->getPieceLocation(), this->_board, &toDeleteTemp); //Checks if the piece can get to the king...
					this->_board[i][j]->setPieceLocation(originalPieceLocation);
				}
				catch (const ChessException& error) //This piece couldn't eat the king... he is not being threatened by this piece
				{
					if (error.getExceptionCode() == Move::VALID)
					{
						return true;
					}
				}
				catch (...)
				{
					std::cout << "Catched an unknown exception at \"GameBoard.cpp\" in \"isKingChecked\" function. continuing..." << std::endl;
				}
			}
		}
	}

	return false;
}


void GameBoard::inBounds(const string& location) const
{
	if ((location[SRC_CHAR] < 'a' || location[SRC_CHAR] > 'h') || (location[SRC_INT] < BOARD_START || location[SRC_INT] > BOARD_SIZE))
	{
		throw ChessException(Move::INDEX_OUT_OF_BOUNDS);
	}
}

void GameBoard::destHasOppositeColor(const string& location, const Color& currPlayingColor) const
{
	Piece* pieceToBeEaten = this->getPieceByLocation(location);

	if (pieceToBeEaten->getPieceColor() == currPlayingColor)
	{
		throw ChessException(Move::SAME_COLOR_PIECE_AT_DEST);
	}
}

void GameBoard::srcHasWantedColor(const string& location, const Color& currPlayingColor) const
{
	Piece* pieceToBePlayed = this->getPieceByLocation(location);

	if (pieceToBePlayed->getPieceColor() != currPlayingColor)
	{
		throw ChessException(Move::OPPOSITE_COLOR_PIECE_AT_SRC);
	}
}

void GameBoard::willMoveMakeKingChecked(const string& move, const Color& currPlayingColor) const
{
	Piece* pieceToMove = nullptr;
	Piece* pieceToDelete = nullptr;
	string oldPieceLocation;
	string srcMove = "";
	string destMove = "";

	srcMove.push_back(move[SRC_CHAR]);
	srcMove.push_back(move[SRC_INT]);
	
	destMove.push_back(move[DEST_CHAR]);
	destMove.push_back(move[DEST_INT]);

	pieceToMove = this->getPieceByLocation(srcMove);
	oldPieceLocation = pieceToMove->getPieceLocation();
	
	try
	{
		pieceToMove->move(destMove, this->_board, &pieceToDelete);
	}
	catch (const ChessException & error)
	{
		if (error.getExceptionCode() == Move::VALID)
		{
			if (this->isKingChecked(currPlayingColor))
			{
				throw ChessException(Move::CHECK_ON_CURR_PLAYER);
			}
		}
		else
		{
			throw error;
		}
	}
}
