#include "Queen.h"

Queen::Queen(const Type& pieceType, const string& pieceLocation, const Color& pieceColor):Piece(pieceType, pieceLocation, pieceColor)
{

}

Queen::~Queen()
{

}

void Queen::move(const string& newPlace, Piece*** board, Piece** pieceToDelete)
{
	if (_pieceLocation == newPlace)
	{
		throw ChessException(Move::SRC_EQU_DEST);
	}

	if (int(_pieceLocation[SRC_CHAR]) - int(newPlace[SRC_CHAR]) == _pieceLocation[SRC_INT] - newPlace[SRC_INT] || // slant right 
		int(newPlace[SRC_CHAR]) - int(_pieceLocation[SRC_CHAR]) == _pieceLocation[SRC_INT] - newPlace[SRC_INT] || // slant left
		_pieceLocation[SRC_CHAR] == newPlace[SRC_CHAR] || _pieceLocation[SRC_INT] == newPlace[SRC_INT])  // forward - backwards / right - left
	{
		_pieceLocation = newPlace;
		throw ChessException(Move::VALID);
	} 

	else
	{
		throw ChessException(Move::INVALID_MOVE_PATTERN);
	}
	
}