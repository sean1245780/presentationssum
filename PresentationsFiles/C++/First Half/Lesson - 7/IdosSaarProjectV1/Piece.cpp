#include "Piece.h"

Piece::Piece(const Type& pieceType, const std::string& pieceLocation, const Color& pieceColor)
{
	this->_pieceType = pieceType;
	this->_pieceLocation = pieceLocation;
	this->_pieceColor = pieceColor;
}

Piece::~Piece()
{

}

Type Piece::getPieceType() const
{
	return this->_pieceType;
}

std::string Piece::getPieceLocation() const
{
	return this->_pieceLocation;
}

Color Piece::getPieceColor() const
{
	return this->_pieceColor;
}

void Piece::setPieceLocation(const std::string& newLocation)
{
	this->_pieceLocation = newLocation;
}

bool operator==(const Piece& p1, const Piece& p2)
{
	if (p1._pieceType == p2._pieceType && p1._pieceLocation == p2._pieceLocation && p1._pieceColor == p2._pieceColor)
	{
		return true;
	}

	return false;
}
