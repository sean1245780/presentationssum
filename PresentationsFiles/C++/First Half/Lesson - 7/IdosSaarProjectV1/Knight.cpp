#include "Knight.h"

Knight::Knight(const Type& pieceType, const string& pieceLocation, const Color& pieceColor):Piece(pieceType, pieceLocation, pieceColor)
{

}

Knight::~Knight()
{

}

void Knight::move(const std::string& newPlace, Piece*** board, Piece** pieceToDelete)
{
	if (_pieceLocation == newPlace)
	{
		throw ChessException(Move::SRC_EQU_DEST);
	}

	if (char( int(_pieceLocation[SRC_CHAR]) + 1) == newPlace[SRC_CHAR] && _pieceLocation[SRC_INT] + 2 == newPlace[SRC_INT] || // two up, one right
	   char( int(_pieceLocation[SRC_CHAR]) - 1) == newPlace[SRC_CHAR] && _pieceLocation[SRC_INT] + 2 == newPlace[SRC_INT] || // two up, one left
       char( int(_pieceLocation[SRC_CHAR]) - 2) == newPlace[SRC_CHAR] && _pieceLocation[SRC_INT] + 1 == newPlace[SRC_INT] || // two left, one up
	   char( int(_pieceLocation[SRC_CHAR]) - 2) == newPlace[SRC_CHAR] && _pieceLocation[SRC_INT] - 1 == newPlace[SRC_INT] || // two left, one down
	   char( int(_pieceLocation[SRC_CHAR]) - 1) == newPlace[SRC_CHAR] && _pieceLocation[SRC_INT] - 2 == newPlace[SRC_INT] || // one left, two down
	   char( int(_pieceLocation[SRC_CHAR]) + 1) == newPlace[SRC_CHAR] && _pieceLocation[SRC_INT] - 2 == newPlace[SRC_INT] || // one right, two down
	   char( int(_pieceLocation[SRC_CHAR]) + 2) == newPlace[SRC_CHAR] && _pieceLocation[SRC_INT] - 1 == newPlace[SRC_INT] || // two right, one down
	   char( int(_pieceLocation[SRC_CHAR]) + 2) == newPlace[SRC_CHAR] && _pieceLocation[SRC_INT] + 1 == newPlace[SRC_INT]) // two right, one up
	{
		_pieceLocation = newPlace;
		throw ChessException(Move::VALID);
	}
	else
	{
		throw ChessException(Move::INVALID_MOVE_PATTERN);
	}
}