#pragma once

#include "EnumUtils.h"
#include <iostream>
#include <string>

using std::string;

//Move instruction indexes
#define SRC_CHAR 0
#define SRC_INT 1
#define DEST_CHAR 2
#define DEST_INT 3

class Piece
{
	public:
		Piece(const Type& pieceType, const string& pieceLocation, const Color& pieceColor);
		virtual ~Piece();

		virtual void move(const string& newPlace, Piece*** board, Piece** pieceToDelete) = 0;
		
		Type getPieceType() const;
		string getPieceLocation() const;
		Color getPieceColor() const;
	
		void setPieceLocation(const string& newLocation);

		friend bool operator==(const Piece& p1, const Piece& p2);

	protected:
		Type _pieceType;
		string _pieceLocation;
		Color _pieceColor;
};
