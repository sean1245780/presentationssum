#pragma once

#include "Piece.h"
#include "ChessException.h"

class Queen : public Piece
{
	public:
		Queen(const Type& pieceType, const string& pieceLocation, const Color& pieceColor);
		virtual ~Queen();
		
		virtual void move(const string& newPlace, Piece*** board, Piece** pieceToDelete);
};