/*********************************
* Class: MAGSHIMIM C1			 *
* Week 3           				 *
* Class example 				 *
* Casting						 *
**********************************/
#include <stdlib.h>
#include <stdio.h>

/**
This is the main function of the program.
*/
int main(void)
{
	int a = 65;
	char c = (char)a;
	int m = 3.0/2;
	float df = 54.4;
	
	printf("Test It: %f\n", 5 / 2); 	// It's a writen in a wrong way!
	printf("%c\n", c);				// output: A
	printf("%f\n", (float)a);   	// output: 65.000000
	printf("%f\n", 5.0 / 2);		// output: 2.5000000
	printf("%f\n", 5 / 2.0);		// output: 2.5000000
	printf("%f\n", (float)5 / 2); 	// output: 2.5000000
	printf("%f\n", 5 / (float)2); 	// output: 2.5000000
	printf("%f\n", (float)(5 / 2)); // output: 2.0000000 - we cast only after division and result was 2
	printf("%f\n", 5.0 / 2); 		// output: 2.5000000
	printf("%d\n", m); 				// output: 1
	

	return 0;
}
