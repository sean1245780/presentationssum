/*********************************
* Class: MAGSHIMIM C1			 *
* Week 3           				 *
* Class example  				 *
* #define					     *
**********************************/
#include <stdio.h>						// Standard input-output library
#include <stdlib.h>						// Standard general utilities library

#define POUND_TO_KILOGRAM_RATIO 0.45359237

/**
The program converts a mass given in kilograms into pounds.
*/
int main(void)
{
	// Regular variables - Read/Write
	float numKilograms = 2.0;

	printf("Input number of kilograms is: %.2f.\n", numKilograms);
	printf("These are equal to %.2f pounds!!!\n", numKilometres * KM_TO_MILE_RATIO);

	return 0;
}
