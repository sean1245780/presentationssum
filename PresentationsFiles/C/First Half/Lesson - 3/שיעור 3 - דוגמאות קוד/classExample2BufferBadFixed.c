#include <stdio.h>

int main(void)
{
	int	age = 0;
	 
	// Buffer: || (empty)
	
	char firstLetter = 'a';
	printf("Please enter your age\n");
	scanf("%d", &age);
	/* Buffer got the user's answer - a number and ENTER (\n)
	   The number is moved into "age" and \n remains in the buffer. 
	*/
	
	// Buffer: |'\n'| 
	
	getchar(); // Cleaning the buffer!
	
	// Buffer: || (empty)
	
	printf("Please enter your first letter\n");
	
	// Buffer is empty - no problems!
	scanf("%c", &firstLetter);
	
	printf("Age is %d and first letter is %c. Nice!\n", age, firstLetter);

	return 0;
}


