#include <stdio.h>

int main(void)
{
	int	age = 0;
	 
	// Buffer: || (empty)
	
	char firstLetter = 'a';
	printf("Please enter your age\n");
	scanf("%d", &age);
	/* Buffer got the user's answer - a number and ENTER (\n)
	   The number is moved into "age" and \n remains in the buffer. 
	*/
	
	// Buffer: |'\n'| 
	
	printf("Please enter your first letter\n");
	/* Buffer contains the ASCII value for \n
	   and this value is moved into "firstLetter"
	   So there is no need to ask the user for a char value - program will continue.
	*/
	scanf("%c", &firstLetter);

	// Buffer: || (empty)
	
	printf("Age is %d and first letter is %c. Nice!\n", age, firstLetter);

	return 0;
}


