/*********************************
* Class: MAGSHIMIM C1			 *
* Week 3           				 *
* Class example  				 *
* const						     *
**********************************/
#include <stdio.h>	// Standard input-output library
#include <stdlib.h>	// Standard general utilities library

/**
This is the main function of the program.
*/
int main(void)
{
	// Constants - Read only
	const float DOLLAR_RATE = 3.85;

	// Regular variables - Read/Write
	float ipad6Price = 93.4;		//prices are in dollars!
	float galaxys5Price = 91.2;
	float nexus6Price = 76.7;

	//DOLLAR_RATE = 3.89;			// Compilation error - why?
	ipad6Price = 93.6;			// No compilation error - why?
	
	printf("Ipad6 price in shekels= %.2f\n", ipad6Price * DOLLAR_RATE);
	printf("Galaxys5 price in shekels = %.2f\n", galaxys5Price * DOLLAR_RATE);
	printf("Nexus6 price in shekels = %.2f\n", nexus6Price * DOLLAR_RATE);

	return 0;
}
