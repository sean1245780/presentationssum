/*********************************
* Class: MAGSHIMIM C1			 *
* Week 3           				 *
* Class example 4  				 *
* getch/putch + getchar/putchar  *
**********************************/
#include <stdio.h>	// Standard input-output library
#include <stdlib.h>	// Standard general utilities library

/**
This is the main function of the program.
*/
int main(void)
{
	// variable declaration
	char c = 'a';
	int d = 0;

	// Example: getchar, putchar
	printf("Enter the first letter of your name: ");
	c = getchar();
	printf("The letter is: ");
	putchar(c);

	printf("\n");

	getchar(); // clears the buffer

	printf("enter number: ");
	scanf("%d", &d);
	
	// Example: buffer affects getchar
	printf("Enter the first letter of your name:");
	c = getchar();
	//c contains the '\n' from the scanf we did before.
	printf("The letter is: ");
	putchar(c); //the program will make a new line 

	return 0;
}
