/*********************************
* Class: MAGSHIMIM C1			 *
* Week 3           				 *
* Class example   				 *
* Scanf		                         *
**********************************/
#include <stdio.h>	// Standard input-output library
#include <stdlib.h>	// Standard general utilities library

/**
This is the main function of the program.
*/
int main(void)
{
	int		studentRank = 0;
	float	studentGrade = 0;
	char	studentFirstLetter = 'a';

	
	// First Way
	printf("Please enter student rank:");
	scanf("%d", &studentRank);

	printf("Please enter student grade:");
	scanf("%f", &studentGrade);

	getchar(); // this command clears all the buffer (the Enters are still in the buffer)
	
	printf("Please enter student first letter:");
	scanf("%c", &studentFirstLetter);

	printf("Student details: %d, %.2f, %c\n", studentRank, studentGrade, studentFirstLetter);


	
	// Second way
	printf("Please enter student rank, grade, first letter:");
	
	scanf("%d %f %c", &studentRank, &studentGrade, &studentFirstLetter);

	printf("Student details: %d, %.2f, %c\n", studentRank, studentGrade, studentFirstLetter);


	return 0;
}
