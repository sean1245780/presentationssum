/*********************************
* Class: MAGSHIMIM C1			 *
* Week 8          				 *
* Class Example 	  			 *
* Trying to get random numbers	 *
**********************************/
/*run this code TWICE and see if you get random numbers....*/
//     :D

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int randNum = 0;

	randNum = rand();
	printf("%d\n", randNum);

	return 0;
}

