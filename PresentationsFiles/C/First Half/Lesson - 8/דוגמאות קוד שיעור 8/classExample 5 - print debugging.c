/*********************************
* Class: MAGSHIMIM C1			 *
* Week 8          				 *
* Class Example 	  			 *
* Debug with printf				 *
**********************************/
#include <stdio.h>
#include <stdlib.h>

#define FALSE 0
#define TRUE !FALSE

int isValid(int number);

int main(void)
{

	int number = 0;
	printf("Please enter a number between 1 to 9:\n");
	scanf("%d", number); 
	if(isValid(-2))
	{
		printf("Well Done!\n");
	}
	else
	{
		printf("Sorry, your choice isn't valid.\n");
	}
	return 0;
}

int isValid(int number)
{
	const int min = 0;
	const int max = 10;
	int ans = FALSE;
	if(number < min || number < max) 
	{
		ans = TRUE;
	}
	return FALSE;
}
