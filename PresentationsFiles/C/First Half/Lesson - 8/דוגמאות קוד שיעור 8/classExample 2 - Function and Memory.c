/*********************************
* Class: MAGSHIMIM C1			 *
* Week 8          				 *
* Class Example 	  			 *
* Function with return value	 *
**********************************/

#include <stdio.h>
#include <stdlib.h>

// Function declaration

int changeNum(int x);
void changeNum2(int x);

int main(void)
{
	int x = 3;

	// Functions and memory
	printf("This is x: %d\n",x);
	changeNum2(x);
	printf("This is x after the void function: %d\n",x);
	x = changeNum(x);
	printf("This is x after changeNum: %d\n",x);
	system("PAUSE");
	return 0;
}
	
/*
The function 'changeNum2' adds 1 to the number x it recieves and prints the result

Input:
	x - The input number 
Output:
	None
*/
void changeNum2(int x)
{
	x = x + 1;
	printf("This is x inside changeNum2: %d\n",x);
}

/*
The function 'changeNum' adds 1 to the number x it recieves and returns it.

Input:
	x - The input number 
Output:
	x + 1
*/
int changeNum(int x)
{
	x = x + 1;
	return x;
}

