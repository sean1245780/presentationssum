/*********************************
* Class: MAGSHIMIM C1			 *
* Week 8          				 *
* Getting random numbers (srand) *
**********************************/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
	int randNum = 0;
	
	srand (time(NULL));
	
	randNum = rand();
	printf("%d\n", randNum);

	return 0;
}


