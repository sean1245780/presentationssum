/*********************************
* Class: MAGSHIMIM C1			 *
* Week 8          				 *
* Class Example 	  			 *
* Function with return value	 *
**********************************/

#include <stdio.h>
#include <stdlib.h>

void addAndPrint(int a, int b);
int add(int a, int b);
int multiply(int a, int b);

int main(void)
{
	int sum = 0;
	int x = 6;
	int prod = 0;
	
	addAndPrint(7,8);
	
	sum = add(20, 1);
	printf("Sum is %d\n", sum);
	
	printf("Another way: %d\n", add(x,4));
	
	sum = add(20, add(5,30));
	printf("Super sum: %d\n", sum);
	
	prod = multiply(4,7);
	printf("4 * 7 = %d\n", prod);
	
	return 0;
}	
	
	
/*
Function will add two numbers and print result
input: the two numbers
output: none
*/
void addAndPrint(int a, int b)
{
	printf("I am addAndPrint. Result is %d\n", a+b);
}

/*
Function will add two numbers and return result.
input: the two numbers
output: sum of the two numbers
*/
int add(int a, int b)
{
	int result = a+b;
	return result;
}


/*
Function will multiply two numbers and return result.
input: the two numbers
output: product of the two numbers
*/
int multiply(int a, int b)
{	
	int i = 0, product = 0;
	for(i = 0; i < a; i++)
	{
		product = add(product,b);
	}
	return product;
	/*
	also possible of course to write this  function using 1 line:
	return a*b;
	*/
}