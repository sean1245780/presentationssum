/*********************************
* Class: MAGSHIMIM C1			 *
* Week 9           				 *
**********************************/

#include <stdio.h>
#include <stdlib.h>

// Global variables decelerations
int globalNum = 0;

// Functions deceleration
void addOneToGlobal(void);

int main(void)
{
	globalNum = 10; // can access it here!
	int i = 0; //local variable
    for (i = 0; i < 6; ++i)
	{
        addOneToGlobal();
		printf("globalNum is now %d\n", globalNum);
	}

	return 0;
}

/*
Function will add one to the global variable
input: none
output: none
*/
void addOneToGlobal(void)
{
	globalNum++; // can access it here as well!!
}