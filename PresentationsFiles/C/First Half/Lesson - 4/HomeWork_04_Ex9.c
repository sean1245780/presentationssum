#include <stdio.h>

int main(void)
{
	const int minAge = 16;
	const int maxAge = 18;
	int age = 0;
	
	printf("Please enter an age:\n");
	scanf("%d", &age);
	getchar(); //cleaning the buffer

	// Hint: check the paranthesis 
	if(!age >= minAge || age <= maxAge)
		printf("Yay! your age is not between 16 and 18!\n");
		printf("You can have a phone!\n");
	
	printf("Sorry, no phone for you...\n");
	
	return 0;
}
 
