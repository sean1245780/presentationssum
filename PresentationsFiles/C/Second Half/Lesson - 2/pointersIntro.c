/*********************************
* Class: MAGSHIMIM C2			 *
* Week 2           				 *
* HW solution   			 	 *
**********************************/
#include <stdio.h>

/*Your answer*/

int main(void)
{
    char x = 0;
    char* px = 0;
    px = &x;
    x = 'a';
    *px = 'b';
    printf("%p %c \n", &x, x);        
    printf("%p %c \n", px, *px);
  return 0;
}
