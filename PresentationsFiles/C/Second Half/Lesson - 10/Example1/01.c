/*********************************
* Class: MAGSHIMIM C2			 *
* Week 10              			 *
* Class Example - Linked List	 *
**********************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STR_LEN 20

typedef struct SongNode
{
	char name[STR_LEN];
	char artist[STR_LEN];
	int duration;
	struct SongNode* next;
}SongNode;

SongNode* createSong(char name[], char artist[], int duration);

int main(void)
{
	SongNode* first = NULL;
	SongNode* second = NULL;
	SongNode* third = NULL;

	first = createSong("Etze li hashuka", "Shefita", 123);
	second = createSong("Sara", "Bob Dylan", 327);
	third = createSong("Tziona", "Nisim Gareme", 283);

	first->next = second;
	second->next = third;
	


	getchar();	
	return 0;
}



/**
Function will create a song
input:
song name, artist name, and its duration
output:
the song updated with correct information
*/
SongNode* createSong(char name[], char artist[], int duration)
{
	SongNode* newSong = (SongNode*)malloc(sizeof(SongNode));

	strncpy(newSong->name, name, STR_LEN);
	strncpy(newSong->artist, artist, STR_LEN);
	newSong->duration = duration;
	newSong->next = NULL;

	return newSong;
}
