struct file
{
	mode_t f_mode;
	loff_t f_pos;
	unsigned short f_flags;
	unsigned short f_count;
	unsigned long f_reada, f_ramax, f_raend, f_ralen, f_rawin;
	struct file *f_next, *f_prev;
	int f_owner;         /* pid or -pgrp where SIGIO should be sent */
	struct inode * f_inode;
	struct file_operations * f_op;
	unsigned long f_version;
	void *private_data;  /* needed for tty driver, and maybe others */
};

typedef struct file FILE;

