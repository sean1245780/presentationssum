/*********************************
* Class: MAGSHIMIM C2			 *
* Week 9 - Recursion			 *
* Class Example	 			 	 *
**********************************/

#include <stdio.h>
#include <stdlib.h>


int sumOfNumbers(int num);

int main(void)
{
	int a = 0;
	int sum = 0;
	printf("Enter number: ");
	scanf("%d", &a);
	sum = sumOfNumbers(a);
	printf("res = %d\n", sum);

	getchar();
	return 0;
}

/*
Function will calculate sum of numbers from 1 to given number
input: the number of sum up to
output: the sum
*/
int sumOfNumbers(int num)
{
	int answer = 0;
	if (num == 0)
	{
		// we�ve reached the end
		answer = 0;
	}
	else
	{
		// recursion is still running
		answer = num + sumOfNumbers(num - 1);
	}
	return answer;
}