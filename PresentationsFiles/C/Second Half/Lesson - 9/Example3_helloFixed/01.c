/*********************************
* Class: MAGSHIMIM C2			 *
* Week 9 - Recursion			 *
* Class Example	 			 	 *
**********************************/

#include <stdio.h>
#include <stdlib.h>

void helloRecursive(int howMany);

int main(void)
{
	int a = 0;

	printf("Enter number: ");
	scanf("%d", &a);

	helloRecursive(a);

	return 0;
}

/*
Function prints "Hello Recursive" as many times as asked
input: how many times to print it
output: none
*/
void helloRecursive(int howMany)
{
	if (howMany != 0) // if it is 0, we are done!!!
	{
		printf("Hello Recursive! #%d\n", howMany);
		helloRecursive(howMany - 1); // calling the function with a smaller size
	}
}