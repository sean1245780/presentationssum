/*********************************
* Class: MAGSHIMIM C2			 *
* Week 9 - Recursion			 *
* Class Example	 			 	 *
**********************************/

#include <stdio.h>
#include <stdlib.h>

#define END_OF_LINE -1

int howManyInLine(int * line, int index);

int main(void)
{
	int line[] = { 4, 6, 8, 2, 5, END_OF_LINE };
	int lineSize = howManyInLine(line, 0);
	printf("%d people in line\n", lineSize);
	return 0;
}

/*
Function checks how many people are in the line
input: the line and index of place to count from
output: length of line
*/
int howManyInLine(int * line, int index)
{
	int res = 0;
	if (line[index] == END_OF_LINE) // this is the end of the line!
	{
		res = 0;
	}
	else
	{
		res = 1 + howManyInLine(line, index + 1);
	}
	return res;
}