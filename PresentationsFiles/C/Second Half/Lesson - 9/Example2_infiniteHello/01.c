/*********************************
* Class: MAGSHIMIM C2			 *
* Week 9 - Recursion			 *
* Class Example	 			 	 *
**********************************/

#include <stdio.h>
#include <stdlib.h>


void helloRecursive(void);

int main(void)
{
	helloRecursive();
	return 0;
}

/*
Function prints hello
input: none
output: none
*/
void helloRecursive(void)
{
	printf("Hello Recursive! \n");
	helloRecursive();
}