/*********************************
* Class: MAGSHIMIM C2			 *
* Week 9 - Recursion			 *
* Class Example	 			 	 *
**********************************/

#include <stdio.h>
#include <stdlib.h>

void recursivePrint(int howMany);

int main(void)
{
	int a = 0;

	printf("Enter number: ");
	scanf("%d", &a);

	recursivePrint(a);

	return 0;
}

/*
Function prints start and finish
input: how many times to print it
output: none
*/
void recursivePrint(int howMany)
{
	if (howMany != 0) // if it is 0, we are done!!!
	{
		printf("Start!! #%d\n", howMany);
		recursivePrint(howMany - 1);
		printf("Finish!! #%d\n", howMany);
	}
}