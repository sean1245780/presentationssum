/*********************************
* Class: MAGSHIMIM C2			 *
* Week 1           				 *
**********************************/

#include<stdio.h>
#include<string.h>

#define X_CHAR 'x'
#define O_CHAR 'o'
#define BLANK ' '

#define BOARD_SIZE 3

void printBoard(char board[][BOARD_SIZE]);

int main(void)
{
	char xoBoard[BOARD_SIZE][BOARD_SIZE] = {{BLANK , BLANK , BLANK},
											{BLANK , X_CHAR , BLANK},
											{BLANK , BLANK , BLANK}};
	
	printBoard(xoBoard);
	
	return 0;
}

/*
Function will print the board.
input: the board to print
*/
void printBoard(char board[][BOARD_SIZE])
{
	int i = 0, j = 0;
	for (i = 0; i < BOARD_SIZE; i++)
	{
		for (j = 0; j < BOARD_SIZE; j++)
		{
			printf("%3c", board[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}