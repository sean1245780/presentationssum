/*********************************
* Class: MAGSHIMIM C2			 *
* Week 1               			 *
**********************************/

#include <stdio.h>

int f(int, int);
int g(int, int);
int h(int, int);

int main(void)
{
	f(2, 3);
	g(f(11, 2), h(2, 30));
	f(4, h(5, 71));
	h(f(21, 1), f(2, 2));
	getchar();	
	return 0;
}


int f(int x, int y)
{
	int result = 0;
	if (x > y)
	{
		printf("%d ", x - y);
		result = f(y, x);
	}
	else
	{
		printf("%d ", 2*y - x);
		result = h(x*3, y);
	}
	return result;
}


int g(int a, int b)
{
	int result = 0;
	if (f(a, b) > h(a, b))
	{
		result = h(b, a);
		printf("%d ", result - a*b);
	}
	else
	{
		result = h(a*b, 3);
		printf("%d ", a);
	}
	return result;
}


int h(int num1, int num2)
{
	int result = 0;
	if (num1 > num2)
	{
		printf("%d ", num1*num2);
		result = num2;
	}
	else
	{
		if (num2 != 0)
		{
			result = num1 % num2;
		}
		else
		{
			result = num1 * -1;
		}
	}
	printf("%d ", result);
	return result;
}
