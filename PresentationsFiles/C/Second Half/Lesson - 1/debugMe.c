#include <stdio.h>

void foo(int num);

int main(void)
{
	printf("Hello VS! \n");

	int x = 6; 

	// Mouse over x to see its value
	x = x*x;

	// Enter the function using F11!
	foo(x);

	return 0;
}

void foo(int num)
{	
	num++;
	printf("foo says: %d", num);
}
