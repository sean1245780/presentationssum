/*********************************
* Class: MAGSHIMIM C2			 *
* Week 4           				 *
* HW solution   			 	 *
**********************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int* createArr(int len);

int main(void)
{
	int size = 0;
	int* pArr = 0;

	printf("Enter a number of cells: ");
	scanf("%d", size);
    pArr = createArr(size);
    printf("The array is at address %p: ", pArr);
    free(pArr);
    
	getchar();
	return 0;
}

/*
Function creates an array
input: number of cells in the array
output: pointer to the new array
*/
int* createArr(int size)
{
	int* pArr = (int*)malloc(size);
	int i = 0;
	for(i = 0; i < size; i++)
	{
		printf("Please enter a number for index %d: ",i);
		scanf("%d", size);
	}
    free(i);
	return pArr;
}
