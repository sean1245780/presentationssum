/*********************************
* Class: MAGSHIMIM C2			 *
* Week 3           				 *
* HW solution   			 	 *
**********************************/
#include <stdio.h>
#include <string.h>

void printArray(char* p, int len)
{
	for( p ; p < p + len ; p++ ) 
	{
		printf("%c", *p);
	}
	printf("\n");
}

int main(void)
{
	char* msg = "hi jyegq meet me at 2 :)";
	printArray(msg, strlen(msg));
    getchar();
	return 0;
 }
